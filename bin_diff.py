#!/usr/bin/python3
#
# File: bin_diff.py
# Author: Jules Vranckx
#
# This file has been written as an assignement for the intern position application:
# Binary Diffing: harder, faster, stronger !
#
# Please look at the attached README.md for more indications about installation or execution
#==============================================================================================

#===========
# IMPORT
#===========
import subprocess
import sys

#===========
# GLOBALS
#===========

OBJDUMP_X = "objdump -x "
FUNC_ADDRESS = 0
FUNC_TYPE = 1
FUNC_SIZE = 3
FUNC_NAME = 4

TEXT_INTRO = """
              _
             | |
             | |===( )   //////
             |_|   |||  | o o|
                    ||| ( c  )                  ____
                     ||| \= /                  ||   \_
                      ||||||                   ||     |
                      ||||||                ...||__/|-"
                      ||||||             __|________|__
                        |||             |______________|
                        |||             || ||      || ||
                        |||             || ||      || ||
------------------------|||-------------||-||------||-||-------
                        |__>            || ||      || ||


****************************************************************
** BIN_DIFF: Find differences in your binaries
****************************************************************

Let's see what I can found...
"""

TEXT_SAME_FUNCTIONS = " ** NO DIFFERENCE FOUND"
#===========
# TOOLS
#===========

def execute(command): 
    output = subprocess.getoutput(command)
    return output

def process_objdump_x(data):
    
    return_dic = {}
    elements = [obj.split() for obj in data.split("\n")]
    
    for obj in elements:

        if len(obj) < 4 or obj[FUNC_TYPE] != "F":
            continue
        
        name = obj[FUNC_NAME]
        address = obj[FUNC_ADDRESS]
        size = obj[FUNC_SIZE]

        func = Function(name, address, size)

        return_dic[name] = func

    return return_dic


#===========
# CLASSES
#===========

class Function():
    """
        Represents a function and its parameters

        @param
        name: Function name
        address: Function address
        size: Function size
        code: Function code
    """
    
    def __init__(self, name, address, size):
        """ 
            Initialisation of the object
        """
        self.name = name
        self.address = address
        self.size = size
        
    def add_binary_code(self, code):
        """
            Add binary code to the object for further analysis
        """
        return

    
#===========
# MAIN
#===========

if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("[ERR0] WRONG USAGE: Please use ./bin_diff <binary1> <binary2>")
        exit()
    PROG1 = sys.argv[1]
    PROG2 = sys.argv[2]

    print(TEXT_INTRO)
    print("[] Analysing %s and %s" % (PROG1, PROG2))
    
    # Retrieve main information from "objdump -x"
    prog1_info = execute(OBJDUMP_X + PROG1)
    prog2_info = execute(OBJDUMP_X + PROG2)

    functions = [process_objdump_x(prog1_info), process_objdump_x(prog2_info)]

    # Search for function that are in a file and not in the other
    print("[] Check if there are the same functions in both files")
    diff1 = [func for func in functions[0] if not func in functions[1]]
    diff2 = [func for func in functions[1] if not func in functions[0]]

    if len(diff1) == 0 and len(diff2) == 0:
        print(TEXT_SAME_FUNCTIONS)
    else:
        for func in diff1:
            print(" **" + func + " is in " + PROG1 + ", but not in " + PROG2)
        for func in diff2:
            print(" **" + func + " is in " + PROG2 + ", but not in " + PROG1)

    # Check for function size differences
    print("[] Check if common functions have same size")

        


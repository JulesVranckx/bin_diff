# Assignment

Using the two binaries provided that represent two versions of the same program, find the differences between the two.

Extract all differences that seems relevant to you (from functions names to their content itself). The only constraint is to make that process as automated as possible. 

Additional question: can you identify what has been modified/patched and why ?

# Excepted outcome
The expected output is a script (in any language of your choice) taking the two binaries as input and outputing the list of the differences.

It could be something like the following snippet:
```code
$ python differ.py prog1 prog2

Found `feature X` only in function_1 (prog1) and not in function_2 (prog2).
Found `feature_Y` only in function_X (prog1) and not in function_Y (prog2)
...
```

# Notes
We are more interested by the though process and the methodology rather than a complete exhaustive dump of differences.

